INSERT INTO `product_clienta` (`name`, `description`, `artNumber`, `price`, `stock`, `deleted`, `brand_name`)
VALUES ("Tanggo Original", "Crispy tanggo original flavour", "012345678912", 10000, 200, false, "Orang Tua"),
	   ("Tanggo Chocolate", "Chocolate flavour", "014567098762", 15000, 150, false, "Orang Tua"),
	   ("CranchBox Chocolate", "Thick chocolate flavour", "015234284568", 15000, 100, false, "Orang Tua"),
	   ("CranhBox White", "Vanila flavour", "011234284543", 20000, 150, false, "Orang Tua"),
	   ("Fullo Orignial", "Chocolate flavour", "092342845486", 10000, 350, false, "Orang Tua"),       
	   ("Fullo Strawberry", "Strawberry chocolate flavour", "015234284537", 30000, 150, false, "Orang Tua"), 
	   ("Chitato BBQ", "Rich BBQ flavour", "042342845443", 15000, 150, false, "Indofood"),
	   ("Chitato Sapi Panggang", "Rich Sapi flavour", "079454321445", 15000, 150, false, "Indofood"),
	   ("Chitato Ayam", "Rich Ayam flavour", "078334284530", 12000, 150, false, "Indofood"),
	   ("Chitato Original", "Rich Potato flavour", "024234284569", 15000, 150, false, "Indofood");
       
select * from product_clienta;

INSERT INTO `product_clientb` (`nama`, `deskripsi`, `nomor_artikel`, `harga`, `persediaan_barang`, `dihapus`, `nama_merek`)
VALUES ("Nu Greentea Original", "Fresh green tea", "012345678456", 5000, 200, false, "ABC"),
	   ("Nu Greentea Honey", "Fresh honey green tea", "014567098556", 6000, 150, false, "ABC"),
	   ("Nu Milk Tea Blue", "Thick milk tea", "098234284098", 8000, 100, false, "ABC"),
	   ("Nu Milk Tea Red", "Thick milk tear", "056234284666", 8000, 150, false, "ABC"),
	   ("Nu Zuzu", "Susu anak-anak", "066342845996", 10000, 350, false, "ABC"),       
	   ("Susu Tango", "Susu Tango dari susu sapi ", "029342845372", 9000, 150, false, "Orang Tua"), 
	   ("Vita Jelly Drink", "Diperkaya vitamin", "019342845443", 15000, 150, false, "Orang Tua"),
	   ("Fruit Tea Apple", "Variety of fruit flavors", "029454321777", 15000, 150, false, "Sosro"),
	   ("Fruit Tea Guava", "Variety of fruit flavors", "026334284598", 12000, 150, false, "Sosro"),
	   ("Fruit Tea Berry", "Variety of fruit flavors", "002234284503", 15000, 150, false, "Sosro");
delete from product_clienta;      
select * from product_clientb;

       