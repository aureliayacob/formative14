CREATE TABLE `staging`.`Product_ClientA` (
`id` INT NOT NULL AUTO_INCREMENT,
`name` VARCHAR(45) NOT NULL,
`description` VARCHAR(45),
`artNumber` VARCHAR(12) NOT NULL,
`price` INT NOT NULL,
`stock` INT NOT NULL,
`deleted` BOOLEAN NOT NULL,
`brand_name` VARCHAR(45) NOT NULL,
PRIMARY KEY (`id`));

CREATE TABLE `staging`.`Product_ClientB` (
`id` INT NOT NULL AUTO_INCREMENT,
`nama` VARCHAR(45) NOT NULL,
`deskripsi` VARCHAR(45),
`nomor_artikel` VARCHAR(12) NOT NULL,
`harga` INT NOT NULL,
`persediaan_barang` INT NOT NULL,
`dihapus` BOOLEAN NOT NULL,
`nama_merek` VARCHAR(45) NOT NULL,
PRIMARY KEY (`id`));

CREATE TABLE `production`.`Product` (
`id` INT NOT NULL AUTO_INCREMENT,
`name` VARCHAR(45) NOT NULL,
`description` VARCHAR(45),
`art_number` VARCHAR(12) NOT NULL,
`price` INT NOT NULL,
`stock` INT NOT NULL,
`is_deleted` BOOLEAN NOT NULL,
`brand` VARCHAR(45) NOT NULL,
PRIMARY KEY (`id`));

