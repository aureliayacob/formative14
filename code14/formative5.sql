ALTER TABLE product
ALTER COLUMN brand_id INT NOT NULL;

CREATE TABLE production.brand (
`brand_id` INT NOT NULL AUTO_INCREMENT,
`name` VARCHAR(45) NOT NULL,
PRIMARY KEY(`brand_id`));
