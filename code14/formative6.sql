SET FOREIGN_KEY_CHECKS=0;
ALTER TABLE product
ADD FOREIGN KEY (`brand_id`) REFERENCES `brand`(`brand_id`);

UPDATE product p JOIN brand b ON p.`brand` = b.`name` set p.brand_id = b.`brand_id`;

select * from product;

INSERT INTO production.brand (`name`) VALUES("ABC"),("Sosro"),("Orang Tua"),("Indofood");
select * from brand;
select * from product;